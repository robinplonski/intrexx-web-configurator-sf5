<?php
namespace App\Repository;

use App\Entity\Configuration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\PortalConfiguration;

/**
 * @method PortalConfiguration|null find($id, $lockMode = null, $lockVersion = null)
 * @method PortalConfiguration|null findOneBy(array $criteria, array $orderBy = null)
 * @method PortalConfiguration[]    findAll()
 * @method PortalConfiguration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

class PortalConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PortalConfiguration::class);
    }
}