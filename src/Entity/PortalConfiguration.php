<?php
namespace App\Entity;

use App\Entity\Portal;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PortalConfigurationRepository")
 */
class PortalConfiguration extends AbstractEntity
{
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Portal", inversedBy="portalConfigurations")
     */
    private $portal;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Configuration", mappedBy="portalConfiguration", cascade={"persist"})
     */
    private $configs;

    /**
     * @ORM\Column(type="json")
     */
    private $settings;

    public function __constructor()
    {
        $this->portal = new ArrayCollection();
        $this->configs = new ArrayCollection();
    }

    /**
     * @return Portal
     */
    public function getPortal()
    {
        return $this->portal;
    }

    /**
     * @param Portal $portal
     */
    public function setPortal($portal)
    {
        $this->portal = $portal;
    }

    /**
     * @return Configuration
     */
    public function getConfigs()
    {
        return $this->configs;
    }

    /**
     * @param Configuration $configs
     */
    public function setConfigs($configs)
    {
        $this->configs = $configs;
    }
    public function addConfig(Configuration $config)
    {
        if(!$this->hasConfig($config)){
            $this->configs->add($config);
            $config->setPortalConfigurations($this);
        }
    }

    public function removeConfig(Configuration $config){
        if ($this->hasConfig($config)){
            $this->configs->removeElement($config);
            $config->setPortalConfigurations(null);
        }
    }

    public function hasConfig(Configuration $config)
    {
        $this->configs->contains($config);
    }


    public function getSettings(): ?string
    {
        return $this->settings;
    }

    /**
     * @param PortalConfiguration $settings
     */
    public function setSettings($settings)
    {
        $this->settings = $settings;
    }


}