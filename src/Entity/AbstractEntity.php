<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractEntity
 * @package App\Entity
 * @ORM\MappedSuperclass()
 */

abstract class AbstractEntity
{
    /**
     * @ORM\Id()
     *@ORM\GeneratedValue()
     *@ORM\Column (type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Security\User" )
     */
    private $createdUser;

    /**
     * @ORM\Column(type="dateTime")
     */
    private $createdDate;

    public function __construct()
    {
        $this->createdDate = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return ArrayCollection
     */
    public function getCreatedUser(): ArrayCollection
    {
        return $this->createdUser;
    }

    /**
     * @param ArrayCollection $createdUser
     */
    public function setCreatedUser(ArrayCollection $createdUser): void
    {
        $this->createdUser = $createdUser;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param mixed $createdDate
     */
    public function setCreatedDate($createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    public function isNew(): bool
    {
        return ($this->id == null) ? true : false;
    }

}