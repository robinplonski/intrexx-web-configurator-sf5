<?php
namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity (repositoryClass="App\Repository\ConfigurationRepository")
 */

class Configuration extends AbstractEntity
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\OneToOne (targetEntity="App\Entity\PortalConfiguration", inversedBy="configs")
     */
    private $portalConfigurations;

    public function __construct()
    {
        $this->portalConfigurations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param Configuration $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param Configuration $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return ArrayCollection
     */
    public function getPortalConfigurations()
    {
        return $this->portalConfigurations;
    }

    /**
     * @param ArrayCollection $portalConfigurations
     */
    public function setPortalConfigurations($portalConfigurations)
    {
        $this->portalConfigurations = $portalConfigurations;
    }

    public function hasPortalConfiguration(PortalConfiguration $portalConfiguration)
    {
        $this->portalConfigurations->contains($portalConfiguration);
    }

    public function addPortalConfiguration(PortalConfiguration $portalConfiguration)
    {
        if (!$this->hasPortalConfiguration($portalConfiguration))
        {
            $this->portalConfigurations->add($portalConfiguration);
            $portalConfiguration->setSettings($portalConfiguration);
        }
    }

    public function removePortalConfiguration(PortalConfiguration $portalConfiguration)
    {
        if ($this->hasPortalConfiguration($portalConfiguration))
        {
            $this->portalConfigurations->removeElement($portalConfiguration);
            $portalConfiguration->setSettings(null);
        }
    }


}
