<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PortalRepository")
 */
class Portal extends AbstractEntity
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\PortalConfiguration", mappedBy="portal", cascade={"persist"})
     */
    private $portalConfigurations;

    public function __construct()
    {
        $this->portalConfigurations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getPortalConfigurations()
    {
        return $this->portalConfigurations;
    }

    /**
     * @param ArrayCollection $portalConfigurations
     */
    public function setPortalConfiguration($portalConfigurations)
    {
        $this->portalConfigurations = $portalConfigurations;
    }

    public function addPortalConfiguration(PortalConfiguration $portalConfiguration)
    {
        if (!$this->hasPortalConfiguration($portalConfiguration)){
            $this->portalConfigurations->add($portalConfiguration);
            $portalConfiguration->setPortal($this);
        }
    }

    /**
     * @param PortalConfiguration $portalConfiguration
     */
    public function removePortalConfiguration(PortalConfiguration $portalConfiguration)
    {
        if ($this->hasPortalConfiguration($portalConfiguration)){
            $this->portalConfigurations->removeElement($portalConfiguration);
            $portalConfiguration->setPortal(null);
        }
    }

    /**
     * @param PortalConfiguration $portalConfiguration
     * @return bool
     */
    public function hasPortalConfiguration(PortalConfiguration $portalConfiguration)
    {
        return $this->portalConfigurations->contains($portalConfiguration);
    }


}
